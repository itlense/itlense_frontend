angular
.module('itApp.service.vacancy', [])
.factory('VacancyService', function($resource) {
    return $resource(baseUrl + 'vacancies/', {
        limit: '@limit'
    }, {
        query: {
            method: 'GET', 
            isArray: false
        }
    });
});