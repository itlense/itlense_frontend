'use strict';
var itLense = angular.module('itApp', [
    'ngRoute', 'ngResource', 'angular-loading-bar', 'ngAnimate', 'infinite-scroll', 'ngSanitize', 'ngMap', 'oi.select',
    'itApp.service.vacancy',
    'itApp.vacancy.list',
    'itApp.directive.tagList'
]);

itLense.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/main.html',
        controller: 'mainController',
        title: 'Main'
    }).when('/locations', {
        templateUrl: 'views/cities.html',
        controller: 'citiesController',
        title: 'Cities'
    }).when('/employers', {
        templateUrl: 'views/employers.html',
        controller: 'employersController',
        title: 'Employers'
    }).when('/employers/:id', {
        templateUrl: 'views/employer.html',
        controller: 'employerController',
        title: 'Employer'
    }).when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'profileController',
        title: 'Profile'
    });
});

var baseUrl = 'http://46.101.223.61/api/';

itLense.filter('startFrom', function() {
    return function(input, start) {
        if (!input || !input.length) {
            return;
        }
        start = +start; //parse to int
        return input.slice(start);
    }
});
itLense.filter('htmlToPlaintext', function() {
    return function(text) {
        return text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});
itLense.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});
itLense.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}]);