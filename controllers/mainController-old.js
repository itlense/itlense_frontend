itLense.controller('mainController', function($scope, $http, $location, $timeout, $routeParams, $q, Vacancy) {
    $scope.isRemote = loadVacancies;
    $scope.isRelocate = loadVacancies;
    $scope.remote_work = false;

    Array.prototype.inArray = function(cmp){
        for(var i=0; i < this.length; i++){
            if(cmp(this[i]))
                return true;
        }
        return false;
    }

    Array.prototype.pushIfNotExist = function(obj, cmp){
        if(!this.inArray(cmp)){
            this.push(obj);
            return true;
        }
        return false;
    }


    $http.get(baseUrl + '/tags/?featured=1').
        success(function (response) {
            $scope.featuredTags = response.content
        });

    $http.get(baseUrl + '/locations/?featured=1').
        success(function (response) {
            $scope.featuredCities = response.content
        });

    var url = {
        tags: makeArray($routeParams.tags) || [],
        locations: makeArray($routeParams.locations) || [],
        url: baseUrl + 'vacancies/'
    };

    $scope.tagsArray = function(query, querySelectAs) {
        return getTags(query);
    };

    $scope.featuredTagClick = function(obj){

        if ($scope.tags.pushIfNotExist(obj, function(item){
            return obj.name == item.name;})){
            loadVacancies();
        }

    }

    $scope.featuredCityClick = function(obj){
        if ($scope.locations.pushIfNotExist(obj, function(item){
            return obj.name == item.name;})){
            loadVacancies();
        }
    }

    function getTags(query) {
        var deferred = $q.defer();
        $http.get(baseUrl + 'tags/web/' + query).success(function(data) {
            deferred.resolve(data.results);
        }).error(function(msg, code) {
            deferred.reject(msg);
            $log.error(msg, code);
        });
        return deferred.promise;
    }

    $scope.locationsArray = function(query, querySelectAs) {
        return getLocations(query);
    };

    function getLocations(query) {
        var deferred = $q.defer();
        $http.get(baseUrl + 'locations/web/' + query).success(function(data) {
            deferred.resolve(data.results);
        }).error(function(msg, code) {
            deferred.reject(msg);
            $log.error(msg, code);
        });
        return deferred.promise;
    }

    function makeArray(arr) {
        var items = [];
        if (typeof arr === 'undefined') {
            return items; 
        };
        if (arr instanceof Array) {
        items = arr.map(function(item) {
            var newItem = {
                name: item,
            }
            return newItem;
        })
        } else {
            items.push({
                name: arr,
            });
            // console.log(items);
            return items;
        };
        return items;
    }

    function makeSimpleArray(arr) {
        var items = []
        arr.map(function(item) {
            items.push(item['name']);
        })
        return items;
    }

    function loadVacancies(update) {
        update = update || false;

        var tags = makeSimpleArray($scope.tags);
        var locations = makeSimpleArray($scope.locations);
        var s = url.url + '?tags='+ angular.toJson(tags) + "&locations_name=" + angular.toJson(locations) +
            "&allows_remote=" + $scope.remote_work + "&limit=6";
        // console.log(tags.length);
        if (!tags.length && !locations.length && !update) {
            // TODO загрузить продвигаемые вакансии.
            return 0;
        };
        // console.log(s);
        $http.get(s).
        then(function(response) {
            // console.log(response);
//            $location.path('/').search({tags: tags, locations: locations});
            $scope.response = "";
            $scope.response = response;
            console.log(response)
        }, function(response) {});
    };

    $scope.tags = url.tags;
    $scope.locations = url.locations;

    Vacancy.query({limit: 100}, function(data) {
        console.log(data.content)
    });

    $scope.submit = function() {
        loadVacancies();
    };

    $scope.$watch('tags', function(){
        loadVacancies();
    })

    $scope.$watch('locations', function(){
        loadVacancies();
    })

});