itLense.controller('mainController', function($scope, $http, $location, $timeout, $routeParams, $q, VacancyService) {
    $http.get(baseUrl + '/tags/?featured=1').
    success(function (response) {
        $scope.featuredTags = response.content;
    });

    VacancyService.query({limit: 2}, function(data) {
        $scope.vacancies = data.content;
    });
});