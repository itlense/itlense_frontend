
itLense.controller('employersController', function($scope, $http, $routeParams, $location) {

    var url = {
        count: 0,
        limit: 20,
        offset: parseInt($routeParams.offset) || 0,
        url: baseUrl + 'employers/'
    }

    function loadEmploeyrs() {
        var s = url.url + '?offset=' + url.offset + '&limit=' + url.limit;

        $http.get(s).
            then(function(response) {
                $scope.employers = response.data.content;
                $scope.nextEnabled = response.data.content.length < url.limit;
            }, function(response) {
        });
    };

    $scope.previousPage = function() {
        if (url.offset != 0) {
            url.offset -= url.limit;
            loadEmploeyrs();
            $location.path('employers').search({offset: url.offset});    
        };
    };

    $scope.nextPage = function() {
        url.offset += url.limit;
        loadEmploeyrs();
        $location.path('employers').search({offset: url.offset});
    };

    loadEmploeyrs();
    $scope.previousEnabled = url.offset == 0;

});
