angular
.module('itApp.vacancy.list', [])
.controller('vacancyListController', function($scope) {

    $scope.goAhead = function(vacancy) {
    	alert('Откликнутьсяна пердложение ' + vacancy.employer_name)
    };
})
.directive('vacancyList', function() {
	return {
		restrict: 'EA',
		replace: true,
		controller: 'vacancyListController',
		transclude: true,
        template:
            '<div class="vacancy-list">' +
                '<div class="vacancy-item" ng-repeat="vacancy in model">' +
                    '<h4>{{vacancy.title}}</h4>' +
                    '<div class="vacancy-tags">' +
                        '<span class="label label-primary" ng-repeat="tag in vacancy.tags">{{tag}}</span>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="vacancy-description col-md-10" to-html="vacancy | unsafe">{{vacancy.description | htmlToPlaintext}}</div>' +
                        '<div class="col-md-2"><b>{{vacancy.employer_name}}</b></div>' +
                    '</div>' +
                    '<div class="vacancy-date">Дата публикации: {{vacancy.update_date}}</div>' +
                    '<button class="btn btn-success btn-sm" ng-click="goAhead(vacancy)">Откликнуться</button>' +
                '</div>' +
            '</div>',
		scope: {
			model: '='
		}
	};
});