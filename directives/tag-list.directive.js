angular
.module('itApp.directive.tagList', [])
.controller('tagListController', function($scope) {
	$scope.filter = function(tag) {

	}
})
.directive('tagList', function() {
	return {
		restrict: 'EA',
		replace: true,
		require: 'ngModel',
		controller: 'tagListController',
        template:
            '<div class="form-group">' +
                '<label class="control-label">{{title}}</label>' +
                '<div>' +
                    '<span class="label label-info" ng-repeat="tag in model" ng-click="filter(tag)">' +
                    '{{tag.name}}' +
                    '</span>' +
                '</div>' +
            '</div>',
		scope: {
			model: '=',
			title: '@title'
		}
	};
});